# Task 1.1 - Docker



## 1.1 Работа с Docker

1. Создайте папку для проекта и перейдите в нее.
    ```
    mkdir task11
    cd task11
    ```
2. Установите докер на машину.
    ```
    apt update
    apt install -y ca-certificates curl gnupg lsb-release
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    apt update
    apt install -y docker-ce docker-ce-cli containerd.io
    ```
3. Запустите контейнер с nginx скачанным из docker hub. Контейнер должен быть запущен из командной строки с параметром -expose или -p для того чтобы после запуска стартовая страница nginx была доступна из браузера вашего компьютера.
    ```
    docker run -d --name testnginx -p 80:80 nginx
    ```
    ![Docker 1](docker1.png)
4. Запустите контейнер с MySQL скачанным из докер хаба, используйте --volume , -v при запуске контейнера для того чтобы сохранить базу данных жестком диске хоста.
    ```
    mkdir mysql_data
    docker run --name testmysql -e MYSQL_ROOT_PASSWORD=mypassword -v $(pwd)/mysql_data:/var/lib/mysql -p 127.0.0.1:3306:3306 -d mysql
    apt install -y mysql-client
    ```
5. После запуска контейнера подключитесь к базе, создайте нового пользователя и новую базу.
    ```
    mysql -h 127.0.0.1 -u root -pmypassword
    CREATE DATABASE newdb;
    CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
    GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'localhost';
    FLUSH PRIVILEGES;
    ```
    ![Docker 2](docker2.png)

## 1.2 Работа с Dockerfile
1. Создайте файл Dockerfile в корневой папке проекта. 
В качестве базового образа используйте Ubuntu 20.04
    Докерфайл
    ```
    FROM ubuntu:20.04 as builder
    ```
2. Дополните Dockerfile инструкциями из которого при выполнении команды docker build соберется docker образ с установленным Ruby 2.7.2
    Докерфайл
    ```
    FROM ubuntu:20.04 as builder

    ENV TZ=Europe/Minsk
    RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

    RUN apt update
    RUN apt-get install -y --force-yes build-essential curl git
    RUN apt-get install -y --force-yes zlib1g-dev libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt-dev
    RUN apt clean

    RUN git clone https://github.com/rbenv/rbenv.git /root/.rbenv
    RUN mkdir -p /root/.rbenv/plugins
    RUN git clone https://github.com/rbenv/ruby-build.git /root/.rbenv/plugins/ruby-build
    RUN /root/.rbenv/plugins/ruby-build/install.sh
    ENV PATH /root/.rbenv/bin:$PATH
    RUN mkdir -p /opt/.rbenv
    RUN RBENV_ROOT=/opt/.rbenv rbenv install 2.7.2
    RUN RBENV_ROOT=/opt/.rbenv rbenv global 2.7.2
    ENV PATH /opt/.rbenv/versions/2.7.2/bin:$PATH

    FROM ubuntu:20.04
    COPY --from=builder /opt/.rbenv /opt/.rbenv
    ENV PATH /opt/.rbenv/versions/2.7.2/bin:$PATH
    CMD [ "ruby", "-v" ]
    ```
3. После успешной сборки образа, запустите контейнер для выполнения команды ruby -v, для проверки работоспособности ruby.
    Запуск
    ```
    docker build -t myruby .
    docker run -it --rm myruby
    ```
    Скриншот:
    ![Docker 3](docker3.png)

## 1.3 Работа с Docker Compose
1. Установите docker-compose на хост
    ```
    apt install docker-compose-plugin #репозитории уже добавлялись ранее
    ```
2. С помощью docker-compose установите и запустите сайт на Wordpress. Помимо docker-compose.yml файла у вас могут быть другие файлы? необходимые для работы Wordpress, например nginx.conf и другие.
    docker-compose.yml
    ```
    version: '3'

    services:
    db:
        image: mysql:8.0.29
        container_name: db
        restart: unless-stopped
        env_file: .env
        environment:
        - MYSQL_DATABASE=wordpress
        volumes:
        - dbdata:/var/lib/mysql
        networks:
        - app-network
    
    wordpress:
        depends_on:
        - db
        image: wordpress:5.9.3-fpm
        container_name: wordpress
        restart: unless-stopped
        env_file: .env
        environment:
        - WORDPRESS_DB_HOST=db:3306
        - WORDPRESS_DB_USER=$MYSQL_USER
        - WORDPRESS_DB_PASSWORD=$MYSQL_PASSWORD
        - WORDPRESS_DB_NAME=wordpress
        volumes:
        - wordpress:/var/www/html
        networks:
        - app-network

    webserver:
        depends_on:
        - wordpress
        image: nginx:1.21.6
        container_name: webserver
        restart: unless-stopped
        ports:
        - "80:80"
        volumes:
        - wordpress:/var/www/html
        - ./nginx.conf:/etc/nginx/conf.d/default.conf
        networks:
        - app-network

    volumes:
    wordpress:
    dbdata:

    networks:
    app-network:
        driver: bridge  
    ```
    nginx.conf
    ```
    server {
        listen 80;
        listen [::]:80;

        server_name example.com www.example.com;

        index index.php index.html index.htm;

        root /var/www/html;

        location / {
                try_files $uri $uri/ /index.php$is_args$args;
        }

        location ~ \.php$ {
                try_files $uri =404;
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                fastcgi_pass wordpress:9000;
                fastcgi_index index.php;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param PATH_INFO $fastcgi_path_info;
        }

        location ~ /\.ht {
                deny all;
        }

        location = /favicon.ico {
                log_not_found off; access_log off;
        }
        location = /robots.txt {
                log_not_found off; access_log off; allow all;
        }
        location ~* \.(css|gif|ico|jpeg|jpg|js|png)$ {
                expires max;
                log_not_found off;
        }
    }
    ```
    .env
    ```
    MYSQL_ROOT_PASSWORD=rootpw
    MYSQL_USER=newuser
    MYSQL_PASSWORD=password
    ```
    Запуск
    ```
    docker compose up -d
    ```
    Скриншоты
    ![Docker 4](docker4.png)
    ![Docker 5](docker5.png)
