FROM ubuntu:20.04 as builder

ENV TZ=Europe/Minsk
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update
RUN apt-get install -y --force-yes build-essential curl git
RUN apt-get install -y --force-yes zlib1g-dev libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt-dev
RUN apt clean

RUN git clone https://github.com/rbenv/rbenv.git /root/.rbenv
RUN mkdir -p /root/.rbenv/plugins
RUN git clone https://github.com/rbenv/ruby-build.git /root/.rbenv/plugins/ruby-build
RUN /root/.rbenv/plugins/ruby-build/install.sh
ENV PATH /root/.rbenv/bin:$PATH
RUN mkdir -p /opt/.rbenv
RUN RBENV_ROOT=/opt/.rbenv rbenv install 2.7.2
RUN RBENV_ROOT=/opt/.rbenv rbenv global 2.7.2
ENV PATH /opt/.rbenv/versions/2.7.2/bin:$PATH


FROM ubuntu:20.04
COPY --from=builder /opt/.rbenv /opt/.rbenv
ENV PATH /opt/.rbenv/versions/2.7.2/bin:$PATH
CMD [ "ruby", "-v" ]